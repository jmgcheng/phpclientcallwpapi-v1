<?php 
/*session_start();
echo $_SESSION['session_expire'];
session_unset();
echo $_SESSION['session_expire'];*/
?>
<!DOCTYPE html>
<html ng-app="myApp" ng-controller="myController">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<title>PHPClientCallWPApi v1</title>
</head>
<body>

	<header class="l-header">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-7 col-md-offset-2">
					<nav class="nav nav-main">
						<ul>
							<li ng-if="s_access_token == ''">
								<a class="btn btn-primary" href="login.php" >Login</a>
							</li>
							<li ng-if="s_access_token != ''">
								<a class="btn btn-primary" href="#/post-add" >Add Post</a>
							</li>
							<li>
								<a class="btn btn-primary" href="#/post-listings" >Recent Post</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-md-7 col-md-offset-2">
					<div ng-if="s_access_token == ''" class="alert alert-info" role="alert">
						<p>
							<strong>Login</strong> using username: <strong>guest</strong> and pass: <strong>guest</strong> to add a post.
						</p>
						<br>
						<p>
							<strong>Cookie Problem?</strong>. Just try to login again.
						</p>
					</div>
				</div>
			</div>
		</div>
	</header>




	<div class="l-main">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-7 col-md-offset-2">
					<div ng-view></div>
				</div>
			</div>
		</div>
	</div>

	<footer class="l-footer">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-7 col-md-offset-2">
					<div class="footnote">
						<p>
							ex. PHP HTTP Client connecting to WP Api :: <a href="http://wp-rest-s1.trusted-freelancer.com/" target="_blank">checkout the WP Api <span class="glyphicon glyphicon-new-window" aria-hidden="true"></span></a>
						</p>
					</div>
				</div>
			</div>
		</div>
		
	</footer>


	<script type="text/javascript" src="bower_components/jquery/dist/jquery.js"></script>
	<script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
	<script type="text/javascript" src="bower_components/angular/angular.js"></script>
	<script type="text/javascript" src="bower_components/angular-route/angular-route.js"></script>
	<script type="text/javascript" src="bower_components/angular-sanitize/angular-sanitize.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
</body>
</html>