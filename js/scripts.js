var app = angular.module('myApp', ['ngRoute', 'ngSanitize'])
	.config(function($routeProvider){
		$routeProvider
			.when('/post-listings', {
				templateUrl: 'view-templates/post-listings.html', 
				controller: 'postListingsController'
			})
			.when('/post-add', {
				templateUrl: 'view-templates/post-add.html', 
				controller: 'postAddController'
			})
			.otherwise({
				redirectTo: 'post-listings'
			})
	})
	.controller('myController', ['$scope', '$http', function($scope, $http){
		console.log('myController');
		$scope.a_wpPosts = [];
		$scope.s_access_token = '';


		/**/
		$scope.getSessions = function(){
			$http({
				method  	:'POST',
				url 		:'getSessions.php',
			})
			.then(
				function(res){
					console.log('res success');
					console.log(res);
					$scope.s_access_token = (res.data.access_token != undefined) ? res.data.access_token : '';
				},
				function(res){
					console.log('res fail');
					console.log(res);	
				}
			);
		};
		$scope.getSessions();

	}])
	.controller('postListingsController', ['$scope', '$http', '$location', function($scope, $http, $location){
		console.log('postListingsController');

		/**/
		$scope.getSessions();


		/**/
		$scope.getWpPosts = function(){
			$http({
				method  	:'GET',
				url 		:'http://wp-rest-s1.trusted-freelancer.com/wp-json/wp/v2/posts',
			})
			.then(
				function(o_res){
					console.log('res success');
					console.log(o_res);
					$scope.a_wpPosts = o_res.data;
				},
				function(o_res){
					console.log('res fail');
					console.log(o_res);	
				}
			);
		};
		$scope.getWpPosts();

		


	}])
	.controller('postAddController', ['$scope', '$http', '$location', function($scope, $http, $location){
		console.log('postAddController');
		$scope.frmAddPostState = '';

		/**/
		$scope.getSessions();


		/**/
		if( $scope.s_access_token == '' )
		{ window.location="login.php"; }


		/**/
		$scope.addPost = function(){

			/*alert($scope.frmAddPost.content);
			return false;*/

			$scope.frmAddPostState = 'sending';

			$http({
				method  : 'POST',
				url 		:'addPost.php',
				/*data		:{
					'title'			: $scope.frmAddPost.title,
					'content'		: $scope.frmAddPost.content,
					'excerpt'		: $scope.frmAddPost.excerpt,
					'status'		: 'publish',
					'access_token'	: s_access_token,
					'token_type'	: 'Bearer',
					'scope'			: 'basic'
				},*/
				data: $.param({
					'content'		: $scope.frmAddPost.content
				}),
				headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
			})
			.then(
				function(res){
					console.log('res success');
					console.log(res);
					$('#frmAddPost .alert').hide();
					if(res.data.result == 'success'){
						console.log('refreshing post');
						$scope.frmAddPostState = 'success';
						$location.path( "/post-listings" );
					}
					else {
						$scope.frmAddPostState = 'fail';
					}
				},
				function(res){
					console.log('res fail');
					console.log(res);	
					$scope.frmAddPostState = 'fail';
				}
			);


		};


	}])
	;