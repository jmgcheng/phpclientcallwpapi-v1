<?php
	/**/
	session_start();


	/**/
	include __DIR__.'/vendor/autoload.php';
	use GuzzleHttp\Client;


	/**/
	define('CLIENT_ID', 'SrT7IkAKAWmKSRSwhxNgR5gYfOV8Uk');
	define('CLIENT_SECRET', 'q7HWoF5cDJ8r7HWF15PcrVj7pozJSV');
	define('BASE_URL_ENCODED', urlencode("http://" . $_SERVER['SERVER_NAME'] . dirname($_SERVER['PHP_SELF'])));
	define('REDIRECT_URI_OAUTH_AUTHORIZE', BASE_URL_ENCODED.'/login.php');
	define('OAUTH_AUTHORIZE_URL', 'http://wp-rest-s1.trusted-freelancer.com/oauth/authorize?response_type=code&client_id='.CLIENT_ID.'&redirect_uri='.REDIRECT_URI_OAUTH_AUTHORIZE);
	define('OAUTH_TOKEN_URL', 'http://wp-rest-s1.trusted-freelancer.com/oauth/token');


	/**/
	if( !isset($_GET['code']) ) {
		// requesting authorization code
		$client = new \GuzzleHttp\Client();
		$res = $client->request('GET', OAUTH_AUTHORIZE_URL);
		//echo $res->getStatusCode();
		//echo '<br>';
		echo $res->getBody();
		echo '<script type="text/javascript" src="bower_components/jquery/dist/jquery.js"></script>';
		echo '<script>$(document).ready(function(){ $("head").append( "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />" ); });</script>';
		exit();
	}
	elseif( isset($_GET['code']) && !empty($_GET['code']) ) {
		$client = new \GuzzleHttp\Client();
		$res = $client->request('POST', OAUTH_TOKEN_URL, [
		    'form_params' => [
		        'client_id' => CLIENT_ID,
				'client_secret' => CLIENT_SECRET,
				'grant_type' => 'authorization_code',
				'code' => $_GET['code'],
				'redirect_uri' => REDIRECT_URI_OAUTH_AUTHORIZE
		    ]
		]);
		//echo $res->getStatusCode();
		//echo '<br>';
		//print_r( json_decode($res->getBody()) );

		$o_result = json_decode($res->getBody());
		$_SESSION['access_token'] = ( isset($o_result->access_token) && !empty($o_result->access_token) ) ? $o_result->access_token : '';
		$_SESSION['token_type'] = ( isset($o_result->token_type) && !empty($o_result->token_type) ) ? $o_result->token_type : '';
		$_SESSION['scope'] = ( isset($o_result->scope) && !empty($o_result->scope) ) ? $o_result->scope : '';
		$_SESSION['refresh_token'] = ( isset($o_result->refresh_token) && !empty($o_result->refresh_token) ) ? $o_result->refresh_token : '';
		$_SESSION['session_expire'] = strtotime("+12 minutes");

		/*
			everythings done? Redirect back to index
		*/
		header('Location: ' . "http://" . $_SERVER['SERVER_NAME'] . dirname($_SERVER['PHP_SELF']));
    	exit();
	}
	else {
		echo 'else failed';
		exit();
	}

?>